package com.detroitlabs.weather.presenter;

import com.detroitlabs.weather.model.Weather;
import com.detroitlabs.weather.api.WeatherAPIInteractor;
import com.detroitlabs.weather.view.CurrentView;
import com.detroitlabs.weather.view.ForecastView;

import org.jetbrains.annotations.NotNull;

import javax.inject.Inject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class WeatherPresenter {
    private CurrentView mCurrentView;
    private ForecastView mForecastView;
    private WeatherAPIInteractor mInteractor;

    @Inject
    public WeatherPresenter(WeatherAPIInteractor interactor) {
        this.mInteractor = interactor;
    }

    public void bind(CurrentView view) { this.mCurrentView = view; }
    public void bind(ForecastView view) {
        this.mForecastView = view;
    }

    public void unbind(CurrentView view) {
        mCurrentView = null;
    }

    public void unbind(ForecastView view) {
        mForecastView = null;
    }

    public void getForecast(double lat, double lng) {
        mInteractor.getForecast(lat, lng)
            // enqueue runs the request on a separate thread
            .enqueue(new Callback<Weather>() {
                // We receive a Response with the content we expect already parsed
                @Override
                public void onResponse(@NotNull Call<Weather> call,
                                       @NotNull Response<Weather> weather) {
                    if (mForecastView != null)
                        mForecastView.updateUi(weather.body());
                }

                // In case of error, this method gets called
                @Override
                public void onFailure(@NotNull Call<Weather> call, @NotNull Throwable t) {
                    t.printStackTrace();
                    if (mForecastView != null)
                        mForecastView.updateUi(null);
                }
            });
    }

    public void getCurrent(double lat, double lng) {
        mInteractor.getCurrent(lat, lng)
            .enqueue(new Callback<Weather>() {
                // We receive a Response with the content we expect already parsed
                @Override
                public void onResponse(@NotNull Call<Weather> call,
                                       @NotNull Response<Weather> weather) {
                    if (mCurrentView != null)
                        mCurrentView.updateUi(weather.body());
                }

                // In case of error, this method gets called
                @Override
                public void onFailure(@NotNull Call<Weather> call, @NotNull Throwable t) {
                    t.printStackTrace();
                    if (mForecastView != null)
                        mForecastView.updateUi(null);
                }
            });
    }
}
