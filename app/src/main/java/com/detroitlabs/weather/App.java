package com.detroitlabs.weather;

import android.app.Application;
import android.content.Context;
import android.support.multidex.MultiDexApplication;

public class App extends MultiDexApplication {
    private AppComponent appComponent;

    @Override
    public void onCreate() {
        super.onCreate();
        appComponent = DaggerAppComponent.create();
    }
    public AppComponent getAppComponent() {
        return appComponent;
    }
}
