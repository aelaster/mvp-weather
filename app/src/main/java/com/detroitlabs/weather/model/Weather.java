package com.detroitlabs.weather.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class Weather {

    @SerializedName("name")
    @Expose
    private String city_name = "";

    @SerializedName("dt")
    @Expose
    private int date = 0;

    @SerializedName("weather")
    @Expose
    private ArrayList<WeatherInfo> weatherInfo;

    @SerializedName("list")
    @Expose
    private ArrayList<Weather> forecastInfo;

    @SerializedName("main")
    @Expose
    private TemperatureInfo temperatureInfo;

    public String getCityName() {
        return city_name;
    }

    public int getUTCDate(){
        return date;
    }

    public String getDate(boolean forecastFlag) {
        DateFormat outputFormatter;
        if (forecastFlag) {
            outputFormatter = new SimpleDateFormat("MM/dd/yyyy", Locale.US);
        }else{
            outputFormatter = new SimpleDateFormat("MM/dd/yyyy h:mm a", Locale.US);
        }
        Date theDate = new Date(date * 1000L);
        return outputFormatter.format(theDate);
    }

    public void setDate(int date) {
        this.date = date;
    }

    public ArrayList<WeatherInfo> getWeatherInfo() {
        return weatherInfo;
    }

    public void setWeatherInfo(ArrayList<WeatherInfo> weather_info) {
        this.weatherInfo = weather_info;
    }

    public ArrayList<Weather> getForecastInfo() {
        DateFormat outputFormatter = new SimpleDateFormat("MM/dd/yyyy", Locale.US);
        ArrayList<Weather> dailyForecast = new ArrayList<>();

        //need to figure out what goes into the new weather array
        double tempHigh_holder = 0;
        double tempLow_holder = 0;
        String icon_holder = "";
        int date_holder = 0;
        String date_holder_output = "";
        boolean finalFlag = false;

        Date currentTime = Calendar.getInstance().getTime();

        //this is the number of loop iterations for a given day.
        //given their api, there are a maximum of 8 responses per day.
        //this helps us to pick the icon
        int day_counter = 0;

        for (int i = 0; i < forecastInfo.size() + 1; i++) {
            Weather currentRecord = null;
            Date currentRecord_Date = null;

            if (i < forecastInfo.size()) {
                currentRecord = forecastInfo.get(i);
                currentRecord_Date = new Date(currentRecord.getUTCDate() * 1000L);
            } else {
                finalFlag = true;
            }

            if (tempHigh_holder == 0 && tempLow_holder == 0 && currentRecord != null) {
                tempHigh_holder = currentRecord.getTemperatureInfo().getTemperature_HighAsDegrees();
                tempLow_holder = currentRecord.getTemperatureInfo().getTemperature_LowAsDegrees();
                icon_holder = currentRecord.getWeatherInfo().get(0).getIcon();
                date_holder = currentRecord.getUTCDate();
                date_holder_output = outputFormatter.format(currentRecord_Date);
            } else {
                if (finalFlag || !outputFormatter.format(currentRecord_Date).equals(date_holder_output)) {
                    //new date
                    //check if this is today's date.  if so, skip it for the forecast, per the specs
                    if (!outputFormatter.format(currentTime).equals(date_holder_output)) {
                        //add the holders to the dailyforecast list
                        Weather newWeatherObject = new Weather();
                        newWeatherObject.setDate(date_holder);

                        ArrayList<WeatherInfo> newWeatherInfoList = new ArrayList<>();
                        WeatherInfo newWeatherInfoObject = new WeatherInfo();
                        newWeatherInfoObject.setIcon(icon_holder);
                        newWeatherInfoList.add(newWeatherInfoObject);
                        newWeatherObject.setWeatherInfo(newWeatherInfoList);

                        TemperatureInfo newTemperatureInfoObject = new TemperatureInfo();
                        newTemperatureInfoObject.setTemperatureHigh(tempHigh_holder);
                        newTemperatureInfoObject.setTemperatureLow(tempLow_holder);
                        newWeatherObject.setTemperatureInfo(newTemperatureInfoObject);

                        dailyForecast.add(newWeatherObject);
                    }

                    if (!finalFlag) {
                        //set up the holders again
                        tempHigh_holder = currentRecord.getTemperatureInfo().getTemperature_HighAsDegrees();
                        tempLow_holder = currentRecord.getTemperatureInfo().getTemperature_LowAsDegrees();
                        icon_holder = currentRecord.getWeatherInfo().get(0).getIcon();
                        date_holder = currentRecord.getUTCDate();
                        date_holder_output = outputFormatter.format(currentRecord_Date);
                        day_counter = 0;
                    }
                } else {
                    //compare highs and lows and replace them
                    if (tempHigh_holder < currentRecord.getTemperatureInfo().getTemperature_HighAsDegrees()) {
                        tempHigh_holder = currentRecord.getTemperatureInfo().getTemperature_HighAsDegrees();
                    }

                    if (tempLow_holder > currentRecord.getTemperatureInfo().getTemperature_LowAsDegrees()) {
                        tempLow_holder = currentRecord.getTemperatureInfo().getTemperature_LowAsDegrees();
                    }

                    if (day_counter == 4) {
                        icon_holder = currentRecord.getWeatherInfo().get(0).getIcon();
                    }
                    day_counter++;
                }
            }
        }

        return dailyForecast;
    }

    public void setForecastInfo(ArrayList<Weather> forecast_info) {
        this.forecastInfo = forecast_info;
    }

    public TemperatureInfo getTemperatureInfo() {
        return temperatureInfo;
    }

    public void setTemperatureInfo(TemperatureInfo main_info) {
        this.temperatureInfo = main_info;
    }
}

