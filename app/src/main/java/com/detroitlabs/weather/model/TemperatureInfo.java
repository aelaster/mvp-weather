package com.detroitlabs.weather.model;

import android.content.Context;

import com.detroitlabs.weather.App;
import com.detroitlabs.weather.R;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class TemperatureInfo {
    @SerializedName("temp")
    @Expose
    private double temperature;

    @SerializedName("temp_max")
    @Expose
    private double temperature_high;

    @SerializedName("temp_min")
    @Expose
    private double temperature_low;

    public String getTemprature(Context context) {
        int theTemp = (int) Math.round(temperature);
        return context.getString(R.string.degrees, theTemp);
    }
    public void setTemperature(double temperature_high) {
        this.temperature_high = temperature_high;
    }

    public int getTemperature_HighAsDegrees(){
        return (int)temperature_high;
    }
    public String getTemperature_High(Context context) {
        int theTemp = (int) Math.round(temperature_high);
        return context.getString(R.string.degrees, theTemp);
    }
    public void setTemperatureHigh(double temperature_high) {
        this.temperature_high = temperature_high;
    }

    public int getTemperature_LowAsDegrees(){
        return (int)temperature_low;
    }
    public String getTemperature_Low(Context context) {
        int theTemp = (int) Math.round(temperature_low);
        return context.getString(R.string.degrees, theTemp);
    }
    public void setTemperatureLow(double temperature_low) {
        this.temperature_low = temperature_low;
    }
}