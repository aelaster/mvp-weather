package com.detroitlabs.weather.api;

import com.detroitlabs.weather.model.Weather;

import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class WeatherAPIInteractorImpl implements WeatherAPIInteractor {
    private WeatherAPI mService;

    public WeatherAPIInteractorImpl() {
        // Configure Retrofit
        Retrofit retrofit = new Retrofit.Builder()
                // Base URL can change for endpoints (dev, staging, live..)
                .baseUrl("https://api.openweathermap.org/data/2.5/")
                // Takes care of converting the JSON response into java objects
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        // Create the WeatherAPI Service
        mService = retrofit.create(WeatherAPI.class);
    }

    @Override
    public Call<Weather> getCurrent(double lat, double lng) {
        return mService.getCurrent(lat, lng, "620a19aa6182c4ed7e6d7a09195d7959", "imperial");
    }

    @Override
    public Call<Weather> getForecast(double lat, double lng) {
        return mService.getForecast(lat, lng, "620a19aa6182c4ed7e6d7a09195d7959", "imperial");
    }
}
