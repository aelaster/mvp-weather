package com.detroitlabs.weather.api;

import com.detroitlabs.weather.api.WeatherAPIInteractor;
import com.detroitlabs.weather.api.WeatherAPIInteractorImpl;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Singleton
@Module
public class WeatherModule {
    @Provides
    public WeatherAPIInteractor providesWeatherAPIInteractor() {
        return new WeatherAPIInteractorImpl();
    }
}