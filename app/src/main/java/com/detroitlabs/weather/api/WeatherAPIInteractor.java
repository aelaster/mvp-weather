package com.detroitlabs.weather.api;
import com.detroitlabs.weather.model.Weather;

import retrofit2.Call;

public interface WeatherAPIInteractor {
    Call<Weather> getCurrent(double lat, double lng);
    Call<Weather> getForecast(double lat, double lng);
}
