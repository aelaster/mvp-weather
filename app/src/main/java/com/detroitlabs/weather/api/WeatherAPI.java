package com.detroitlabs.weather.api;

import com.detroitlabs.weather.model.Weather;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface WeatherAPI {
    @GET("weather")
    Call<Weather> getCurrent(@Query("lat") double latitude, @Query("lon") double longitude, @Query("appid") String apiKey, @Query("units") String units);

    @GET("forecast")
    Call<Weather> getForecast(@Query("lat") double latitude, @Query("lon") double longitude, @Query("appid") String apiKey, @Query("units") String units);
}