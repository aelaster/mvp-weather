package com.detroitlabs.weather;

import com.detroitlabs.weather.api.WeatherModule;
import com.detroitlabs.weather.view.CurrentFragment;
import com.detroitlabs.weather.view.ForecastFragment;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = WeatherModule.class)
public interface AppComponent {
    void inject(CurrentFragment frag);
    void inject(ForecastFragment frag);
}
