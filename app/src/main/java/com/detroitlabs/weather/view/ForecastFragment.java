package com.detroitlabs.weather.view;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.detroitlabs.weather.App;
import com.detroitlabs.weather.R;
import com.detroitlabs.weather.adapter.ForecastAdapter;
import com.detroitlabs.weather.model.Weather;
import com.detroitlabs.weather.presenter.WeatherPresenter;

import javax.inject.Inject;

public class ForecastFragment extends Fragment implements ForecastView{
    private WeatherPresenter mPresenter;

    private static final String LATITUDE = "latitude";
    private static final String LONGITUDE = "longitude";

    private double mLatitude;
    private double mLongitude;

    private ForecastAdapter mAdapter;
    private RecyclerView mRecyclerView;

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public ForecastFragment() {
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param lat current latitude.
     * @param lng current longitude.
     * @return A new instance of fragment ForecastFragment.
     */
    @SuppressWarnings("unused")
    public static ForecastFragment newInstance(double lat, double lng) {
        ForecastFragment fragment = new ForecastFragment();
        Bundle args = new Bundle();
        args.putDouble(LATITUDE, lat);
        args.putDouble(LONGITUDE, lng);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
            mLatitude = getArguments().getDouble(LATITUDE);
            mLongitude = getArguments().getDouble(LONGITUDE);
        }
    }

    @Inject
    public void setPresenter(WeatherPresenter presenter) {
        this.mPresenter = presenter;
    }

    @Override
    public void onAttach(Context context) {
        ((App)(context.getApplicationContext())).getAppComponent().inject(this);
        mPresenter.bind(this);
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        mPresenter.unbind(this);
        super.onDetach();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_day_list, container, false);

        // Set the adapter
        if (rootView instanceof RecyclerView) {
            Context context = rootView.getContext();
            mRecyclerView = (RecyclerView) rootView;
            mRecyclerView.setLayoutManager(new LinearLayoutManager(context));
            if (mLatitude != 200 && mLongitude != 200) {
                mPresenter.getForecast(mLatitude, mLongitude);
            }
        }
        return rootView;
    }

    @Override
    public void updateUi(Weather forecast) {
        if (forecast != null){
            mAdapter = new ForecastAdapter(forecast.getForecastInfo(), requireContext());
            mRecyclerView.setAdapter(mAdapter);
        }else{
            Toast toast = Toast.makeText(requireActivity(), getString(R.string.server_connection_error), Toast.LENGTH_SHORT);
            toast.show();
        }
    }
}
