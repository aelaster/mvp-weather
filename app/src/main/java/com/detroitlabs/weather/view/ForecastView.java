package com.detroitlabs.weather.view;

import com.detroitlabs.weather.model.Weather;

public interface ForecastView {
    void updateUi(Weather forecast);
}

