package com.detroitlabs.weather.view;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.detroitlabs.weather.App;
import com.detroitlabs.weather.R;
import com.detroitlabs.weather.model.Weather;
import com.detroitlabs.weather.model.WeatherInfo;
import com.detroitlabs.weather.presenter.WeatherPresenter;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import javax.inject.Inject;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link CurrentFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class CurrentFragment extends Fragment implements CurrentView {
    private WeatherPresenter mPresenter;

    // the fragment initialization parameters
    private static final String LATITUDE = "latitude";
    private static final String LONGITUDE = "longitude";

    private double mLatitude;
    private double mLongitude;

    private TextView mCurrentDate, mCityName, mCurrentTemp, mCurrentConditions;
    private ImageView mWeatherIcon;

    public CurrentFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param lat current latitude.
     * @param lng current longitude.
     * @return A new instance of fragment CurrentFragment.
     */
    public static CurrentFragment newInstance(double lat, double lng) {
        CurrentFragment fragment = new CurrentFragment();
        Bundle args = new Bundle();
        args.putDouble(LATITUDE, lat);
        args.putDouble(LONGITUDE, lng);
        fragment.setArguments(args);
        return fragment;
    }

    @Inject
    public void setPresenter(WeatherPresenter presenter) {
        this.mPresenter = presenter;
    }

    @Override
    public void onAttach(Context context) {
        ((App)(context.getApplicationContext())).getAppComponent().inject(this);
        mPresenter.bind(this);
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        mPresenter.unbind(this);
        super.onDetach();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mLatitude = getArguments().getDouble(LATITUDE);
            mLongitude = getArguments().getDouble(LONGITUDE);
        }
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_current, container, false);

        mCurrentDate = rootView.findViewById(R.id.current_date);
        mCityName = rootView.findViewById(R.id.city_name);
        mCurrentTemp = rootView.findViewById(R.id.current_temp);
        mCurrentConditions = rootView.findViewById(R.id.current_conditions);
        mWeatherIcon = rootView.findViewById(R.id.weather_icon);

        if (mLatitude != 200 && mLongitude != 200) {
            mPresenter.getCurrent(mLatitude, mLongitude);
        }

        return rootView;
    }

    @Override
    public void updateUi(Weather current) {
        if (current != null) {
            mCurrentDate.setText(current.getDate(false));
            mCityName.setText(current.getCityName());
            mCurrentTemp.setText(current.getTemperatureInfo().getTemprature(requireContext()));

            ArrayList<WeatherInfo> weather_info = current.getWeatherInfo();
            mCurrentConditions.setText(getString(R.string.conditions, weather_info.get(0).getDescription(), weather_info.get(0).getDescriptionDetail()));

            Picasso.with(getActivity()).load(getString(R.string.icon_url, weather_info.get(0).getIcon())).into(mWeatherIcon);
        }else{
            Toast toast = Toast.makeText(requireActivity(), getString(R.string.server_connection_error), Toast.LENGTH_SHORT);
            toast.show();
        }
    }
}
