package com.detroitlabs.weather.view;

import com.detroitlabs.weather.model.Weather;

public interface CurrentView {
    void updateUi(Weather current);
}

