package com.detroitlabs.weather.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.detroitlabs.weather.App;
import com.detroitlabs.weather.R;
import com.detroitlabs.weather.model.TemperatureInfo;
import com.detroitlabs.weather.model.Weather;
import com.detroitlabs.weather.model.WeatherInfo;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

public class ForecastAdapter extends RecyclerView.Adapter<ForecastAdapter.ViewHolder> {

    private final List<Weather> mValues;
    private Context mContext;

    public ForecastAdapter(List<Weather> items, Context context) {
        mValues = items;
        mContext = context;
    }

    @Override
    @NonNull
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_day, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, int position) {
        Weather currentWeather = mValues.get(position);
        holder.mDateView.setText(currentWeather.getDate(true));

        TemperatureInfo main_info = currentWeather.getTemperatureInfo();
        holder.mTempHighView.setText(main_info.getTemperature_High(mContext));
        holder.mTempLowView.setText(main_info.getTemperature_Low(mContext));

        ArrayList<WeatherInfo> weather_info = currentWeather.getWeatherInfo();
        Picasso.with(mContext).load(mContext.getString(R.string.icon_url, weather_info.get(0).getIcon())).into(holder.mWeatherIcon);
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        private final ImageView mWeatherIcon;
        private final TextView mDateView;
        private final TextView mTempHighView;
        private final TextView mTempLowView;

        private ViewHolder(View view) {
            super(view);
            mWeatherIcon = view.findViewById(R.id.weather_icon);
            mDateView = view.findViewById(R.id.current_date);
            mTempHighView = view.findViewById(R.id.temp_high);
            mTempLowView = view.findViewById(R.id.temp_low);
        }
    }
}